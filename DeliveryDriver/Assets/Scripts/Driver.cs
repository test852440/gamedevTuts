using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    public bool hasPackage = false;
    public bool speedBack = false;

    [SerializeField] public float steerSpeed = 200f;
    [SerializeField] public float moveSpeed = 10f;
    [SerializeField] public float boostSpeed = 35f;
    [SerializeField] public float bumpSpeed = 5f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float steerAmount = Input.GetAxis("Horizontal")* steerSpeed * Time.deltaTime;
        float moveAmount = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        transform.Rotate(0, 0, -steerAmount);
        transform.Translate(0, moveAmount, 0);

        if (speedBack && moveSpeed > 15f)
        {
            float speeddDif = moveSpeed - 15f;
            moveSpeed-= speeddDif;
            print("less");
        }
        //if (speedBack && moveSpeed > 15f)
        //{
        //    float speeddDif = moveSpeed - 15f;
        //    moveSpeed--;
        //    print("less");
        //}
        else if(moveSpeed == 15f)
        {
            speedBack = false;
        }
    }

    private IEnumerator SpeedBack()
    {
        speedBack = true;
        yield return new WaitForSeconds(10f);
        speedBack = false;
        moveSpeed = 15f;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "package" && !hasPackage)
        {
            this.GetComponent<SpriteRenderer>().color = collision.GetComponent<SpriteRenderer>().color;
            hasPackage = true;
            Destroy(collision.gameObject, 0.5f);
            collision.gameObject.SetActive(false);
        }
        else if (collision.tag == "costumer" && hasPackage)
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            hasPackage = false;
        }

        if (collision.tag == "boost")
        {
            print("sprint");
            moveSpeed = boostSpeed;
            StartCoroutine(SpeedBack());
        }
        if (collision.tag == "bump")
        {
            print("slow");
            moveSpeed = bumpSpeed;
            StartCoroutine(SpeedBack());
        }
    }
}
