using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] public Transform Car;
    void LateUpdate()
    {
        transform.position = new Vector3(Car.position.x, Car.position.y, -10);
        transform.rotation = Car.rotation;
    }
}
